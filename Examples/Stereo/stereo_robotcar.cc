/**
* This file is part of ORB-SLAM3
*
* Copyright (C) 2017-2020 Carlos Campos, Richard Elvira, Juan J. Gómez Rodríguez, José M.M. Montiel and Juan D. Tardós, University of Zaragoza.
* Copyright (C) 2014-2016 Raúl Mur-Artal, José M.M. Montiel and Juan D. Tardós, University of Zaragoza.
*
* ORB-SLAM3 is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
* License as published by the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* ORB-SLAM3 is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
* the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License along with ORB-SLAM3.
* If not, see <http://www.gnu.org/licenses/>.
*/

#include<iostream>
#include<algorithm>
#include<fstream>
#include<iomanip>
#include<chrono>

#include<opencv2/core/core.hpp>

#include<System.h>
#include "ORBVocabulary.h"

using namespace std;

void LoadImages(const string &strPathLeft, const string &strPathRight, const string &strPathTimes,
                vector<string> &vstrImageLeft, vector<string> &vstrImageRight, vector<double> &vTimeStamps);

int main(int argc, char **argv)
{
    bool already_long_paused = false;
    if(argc < 8)
    {
        cerr << endl << "Usage: ./stereo_robotcar path_to_vocabulary path_to_settings path_to_sequence_folder_1 path_to_times_file_1  trajectory_file_name stop_after_initialization_bool split_into_chunks_bool" << endl;

        return 1;
    }

    const int num_seq = 1;
    // cout << "num_seq = " << num_seq << endl;

    string base_output_dirpath = string(argv[5]);
    cout << "Base output dirpath: " << base_output_dirpath << endl;

    // Load all sequences:
    int seq;
    vector< vector<string> > vstrImageLeft;
    vector< vector<string> > vstrImageRight;
    vector< vector<double> > vTimestampsCam;
    vector<int> nImages;

    vstrImageLeft.resize(num_seq);
    vstrImageRight.resize(num_seq);
    vTimestampsCam.resize(num_seq);
    nImages.resize(num_seq);

    int tot_images = 0;
    for (seq = 0; seq<num_seq; seq++)
    {

        string pathSeq(argv[(2*seq) + 3]);
        string pathTimeStamps(argv[(2*seq) + 4]);

        string pathCam0 = pathSeq + "/stereo_undistorted_debayered/left";
        string pathCam1 = pathSeq + "/stereo_undistorted_debayered/right";

        cout << "Loading images from " << pathCam0 << endl;
        cout << "Loading images from " << pathCam1 << endl;
        cout << "Loading timestamps from " << pathTimeStamps << endl;

        LoadImages(pathCam0, pathCam1, pathTimeStamps, vstrImageLeft[seq], vstrImageRight[seq], vTimestampsCam[seq]);

        nImages[seq] = vstrImageLeft[seq].size();
        tot_images += nImages[seq];

        cout << "Loaded " << nImages[seq] << " images!" << endl;
    }

    // Read rectification parameters
    cv::FileStorage fsSettings(argv[2], cv::FileStorage::READ);
    if(!fsSettings.isOpened())
    {
        cerr << "ERROR: Wrong path to settings" << endl;
        return -1;
    }

//     cv::Mat K_l, K_r, P_l, P_r, R_l, R_r, D_l, D_r;
//     fsSettings["LEFT.K"] >> K_l;
//     fsSettings["RIGHT.K"] >> K_r;

//     fsSettings["LEFT.P"] >> P_l;
//     fsSettings["RIGHT.P"] >> P_r;

//     fsSettings["LEFT.R"] >> R_l;
//     fsSettings["RIGHT.R"] >> R_r;

//     fsSettings["LEFT.D"] >> D_l;
//     fsSettings["RIGHT.D"] >> D_r;

//     int rows_l = fsSettings["LEFT.height"];
//     int cols_l = fsSettings["LEFT.width"];
//     int rows_r = fsSettings["RIGHT.height"];
//     int cols_r = fsSettings["RIGHT.width"];

//     if(K_l.empty() || K_r.empty() || P_l.empty() || P_r.empty() || R_l.empty() || R_r.empty() || D_l.empty() || D_r.empty() ||
//             rows_l==0 || rows_r==0 || cols_l==0 || cols_r==0)
//     {
//         cerr << "ERROR: Calibration parameters to rectify stereo are missing!" << endl;
//         return -1;
//     }

//     cv::Mat M1l,M2l,M1r,M2r;
//     cv::initUndistortRectifyMap(K_l,D_l,R_l,P_l.rowRange(0,3).colRange(0,3),cv::Size(cols_l,rows_l),CV_32F,M1l,M2l);
//     cv::initUndistortRectifyMap(K_r,D_r,R_r,P_r.rowRange(0,3).colRange(0,3),cv::Size(cols_r,rows_r),CV_32F,M1r,M2r);


    // Vector for tracking time statistics
    vector<float> vTimesTrack;
    vTimesTrack.resize(tot_images);

    cout << endl << "-------" << endl;
    cout.precision(17);

    bool use_visualizer;
    if (strcmp(argv[6], "true") == 0) {
      use_visualizer = true;
      std::cout << "MCAM: WILL use the visualizer!" << std::endl;
    } else if (strcmp(argv[6], "false") == 0) {
      use_visualizer = false;
      std::cout << "MCAM: Will NOT use the visualizer!" << std::endl;
    } else {
      std::stringstream err_msg;
      err_msg << "Argument 7, to define whether or not to use the visualizer, must be either "
        "\"true\" or \"false\". Argument 7 was: " << argv[6];
      throw std::runtime_error(err_msg.str());
    }

    bool stop_after_initialization;
    if (strcmp(argv[7], "true") == 0) {
      stop_after_initialization = true;
      std::cout << "MCAM: Will stop after initialization!" << std::endl;
    } else if (strcmp(argv[7], "false") == 0) {
      stop_after_initialization = false;
      std::cout << "MCAM: Will NOT stop after initialization!" << std::endl;
    } else {
      std::stringstream err_msg;
      err_msg << "Argument 8, to define whether or not to stop after initialization, must be either "
        "\"true\" or \"false\". Argument 8 was: " << argv[7];
      throw std::runtime_error(err_msg.str());
    }

    bool split_into_chunks;
    const int frames_per_chunk = std::stoi(argv[8]);
    if (frames_per_chunk == -1) {
      split_into_chunks = false;
      std::cout << "MCAM: Will NOT split the data into chunks!" << std::endl;
    } else {
      split_into_chunks = true;
      std::cout << "MCAM: Will split the data into chunks of size " << frames_per_chunk << "!"
                << std::endl;
    }

    int start_frame_idx = std::stoi(argv[9]);
    std::cout << "MCAM: Will start from frame index " << start_frame_idx << "!" << std::endl;

    bool stop_after_first_chunk;
    if (strcmp(argv[10], "true") == 0) {
      stop_after_first_chunk = true;
      std::cout << "MCAM: Will stop after first_chunk!" << std::endl;
    } else if (strcmp(argv[10], "false") == 0) {
      stop_after_first_chunk = false;
      std::cout << "MCAM: Will NOT stop after first_chunk!" << std::endl;
    } else {
      std::stringstream err_msg;
      err_msg << "Argument 11, to define whether or not to stop after initialization, must be "
              << "either \"true\" or \"false\". Argument 11 was: " << argv[10];
      throw std::runtime_error(err_msg.str());
    }

    if (use_visualizer && split_into_chunks && !stop_after_first_chunk) {
      std::cout << "MCAM: Warning - use_visualizer is true, split_into_chunks is true, and "
                << "stop_after_first_chunk is false. The system will probably crash when re-using "
                << "the visualizer on the second chunk.";
    }

    //Load ORB Vocabulary
    cout << endl << "Loading ORB Vocabulary. This could take a while..." << endl;

    const std::string strVocFile = argv[1];
    ORB_SLAM3::ORBVocabulary* mpVocabulary = new ORB_SLAM3::ORBVocabulary();
    bool bVocLoad = mpVocabulary->loadFromTextFile(strVocFile);
    if(!bVocLoad)
    {
        cerr << "Wrong path to vocabulary. " << endl;
        cerr << "Falied to open at: " << strVocFile << endl;
        exit(-1);
    }
    cout << "Vocabulary loaded!" << endl << endl;

    // Chunk size, in frames.
    int frame_idx = start_frame_idx;

    cv::Mat imLeft, imRight, imLeftRect, imRightRect;
    double t_rect = 0;
    int num_rect = 0;
    int num_runs;
    if (split_into_chunks) {
      num_runs = nImages[0] / frames_per_chunk;
    } else {
      num_runs = 1;
    }

    if (stop_after_first_chunk) {
      num_runs = 1;
    }

    for (int run_idx = 0; run_idx < num_runs; ++run_idx) {
      ORB_SLAM3::System SLAM(argv[1],argv[2],ORB_SLAM3::System::STEREO, mpVocabulary,
                             use_visualizer, 0, "", "", "robotcar");
      int run_first_frame_idx = frame_idx;
      int run_last_frame_idx;
      if (split_into_chunks) {
        run_last_frame_idx = run_first_frame_idx + frames_per_chunk - 1;
      } else {
        run_last_frame_idx = nImages[0] - 1;
      }
      for(; frame_idx <= run_last_frame_idx; ++frame_idx) {
        std::cout << "Run: " << (run_idx + 1) << ", Frame: " << (frame_idx + 1) << std::endl;
        // Read left and right images from file
        imLeft = cv::imread(vstrImageLeft[0][frame_idx],cv::IMREAD_UNCHANGED);
        imRight = cv::imread(vstrImageRight[0][frame_idx],cv::IMREAD_UNCHANGED);

        if(imLeft.empty())
        {
            cerr << endl << "Failed to load image at: "
                 << string(vstrImageLeft[0][frame_idx]) << endl;
            return 1;
        }

        if(imRight.empty())
        {
            cerr << endl << "Failed to load image at: "
                 << string(vstrImageRight[0][frame_idx]) << endl;
            return 1;
        }


        #ifdef COMPILEDWITHC11
        std::chrono::steady_clock::time_point t_Start_Rect = std::chrono::steady_clock::now();
        #else
        std::chrono::monotonic_clock::time_point t_Start_Rect = std::chrono::monotonic_clock::now();
        #endif
        // cv::remap(imLeft,imLeftRect,M1l,M2l,cv::INTER_LINEAR);
        // cv::remap(imRight,imRightRect,M1r,M2r,cv::INTER_LINEAR);

        #ifdef COMPILEDWITHC11
        std::chrono::steady_clock::time_point t_End_Rect = std::chrono::steady_clock::now();
        #else
        std::chrono::monotonic_clock::time_point t_End_Rect = std::chrono::monotonic_clock::now();
        #endif

        t_rect = std::chrono::duration_cast<std::chrono::duration<double> >(t_End_Rect - t_Start_Rect).count();
        double tframe = vTimestampsCam[0][frame_idx];


        #ifdef COMPILEDWITHC11
        std::chrono::steady_clock::time_point t1 = std::chrono::steady_clock::now();
        #else
        std::chrono::monotonic_clock::time_point t1 = std::chrono::monotonic_clock::now();
        #endif

        // Pass the images to the SLAM system
        SLAM.TrackStereo(imLeft,imRight,tframe, vector<ORB_SLAM3::IMU::Point>(), vstrImageLeft[0][frame_idx]);

        #ifdef COMPILEDWITHC11
        std::chrono::steady_clock::time_point t2 = std::chrono::steady_clock::now();
        #else
        std::chrono::monotonic_clock::time_point t2 = std::chrono::monotonic_clock::now();
        #endif

        double ttrack= std::chrono::duration_cast<std::chrono::duration<double> >(t2 - t1).count();

        vTimesTrack[frame_idx]=ttrack;

        // Wait to load the next frame
        double T=0;
        if(frame_idx<nImages[0]-1)
            T = vTimestampsCam[0][frame_idx+1]-tframe;
        else if(frame_idx>0)
            T = tframe-vTimestampsCam[0][frame_idx-1];

        // if(ttrack<T)
        //     usleep((T-ttrack)*1e6); // 1e6

        const bool run_in_slow_motion = false;
        if (run_in_slow_motion) {
          if (!already_long_paused) {
            usleep(5e6);
            already_long_paused = true;
          } else {
            usleep(0.5e6);
          }
        }

        bool stop_run = false;

        // Stop running and return results as soon as initialization is complete unless
        // specified otherwise.
        if (stop_after_initialization && SLAM.GetInitializationComplete()) {
          stop_run = true;
        }

        // Stop running if this is the end of a chunk.
        if (split_into_chunks && frame_idx == run_last_frame_idx) {
          stop_run = true;
        }

        // Stop running if this is the last frame.
        if (frame_idx == nImages[0] - 1) {
          stop_run = true;
        }

        if (stop_run) {
          SLAM.Shutdown();

          const int kNumDigits = 5;

          stringstream chunk_start_num_ss;
          chunk_start_num_ss << setfill('0') << setw(kNumDigits) << (run_first_frame_idx + 1);
          stringstream chunk_end_num_ss;
          chunk_end_num_ss << setfill('0') << setw(kNumDigits) << (run_last_frame_idx + 1);

          stringstream f_file_ss;
          f_file_ss << base_output_dirpath << "/" << chunk_start_num_ss.str() << "_"
            << chunk_end_num_ss.str() << "/" << chunk_start_num_ss.str() << "_"
            << chunk_end_num_ss.str() << "_poses.txt";
          // stringstream mp_file_ss;
          // mp_file_ss << "results/" << file_name << "_" << std::setfill('0') << std::setw(kNumDigits)
          //   << (run_first_frame_idx + 1) << "-" << std::setw(kNumDigits) << (run_last_frame_idx + 1)
          //   << "_points.txt";

          SLAM.SaveTrajectoryTUM(f_file_ss.str());
          // SLAM.SaveKeyFrameTrajectoryTUM(f_file_ss.str());
          // SLAM.SaveMapPoints(mp_file_ss.str());
        }
      }
    }

    return 0;
}

void LoadImages(const string &strPathLeft, const string &strPathRight, const string &strPathTimes,
                vector<string> &vstrImageLeft, vector<string> &vstrImageRight, vector<double> &vTimeStamps)
{
    ifstream fTimes;
    fTimes.open(strPathTimes.c_str());
    vTimeStamps.reserve(12000);
    vstrImageLeft.reserve(12000);
    vstrImageRight.reserve(12000);
    while(!fTimes.eof())
    {
        string s;
        getline(fTimes,s);
        if(!s.empty())
        {
            stringstream ss;
            ss << s;
            vstrImageLeft.push_back(strPathLeft + "/" + ss.str() + ".png");
            vstrImageRight.push_back(strPathRight + "/" + ss.str() + ".png");
            double t;
            ss >> t;
            vTimeStamps.push_back(t/1e9);

        }
    }
}
