#!/bin/bash

# Script for running ORB-SLAM3 on a specified EuRoC dataset.
# Usage: ./run-euroc.sh <path_to_euroc_dataset_directory> <use_visualizer>
#        <stop_after_initialization_bool> <split_into_chunks_bool> <starting_frame_index>

# The name of this script.
script_fpath_rel="$0"
# The absolute path to the directory this script is stored in.
script_dirpath=$(dirname $(readlink -f $script_fpath_rel))  # The absolute path

# The path to the EuRoC dataset directory. Make sure the name of the EuRoC dataset directory matches
# the name of the associated ORB-SLAM3 timestamp file in orb-slam3/Examples/Stereo/EuRoC_TimeStamps/
# because the timestamp file is selected based off of the EuRoC dataset directory name.
dataset_dirpath="$1"
dataset_name=$(basename "$dataset_dirpath")

use_visualizer="$2"
stop_after_initialization="$3"
split_into_chunks="$4"
starting_frame_idx="$5"

# Create the command.
cmd="${script_dirpath}/Examples/Stereo/stereo_euroc ${script_dirpath}/Vocabulary/ORBvoc.txt ${script_dirpath}/Examples/Stereo/EuRoC.yaml $dataset_dirpath ${script_dirpath}/Examples/Stereo/EuRoC_TimeStamps/$dataset_name.txt $dataset_name $use_visualizer $stop_after_initialization $split_into_chunks $starting_frame_idx"

# Print the command that is being run.
echo "Running: \"$cmd\""
echo

# Run the command.
$cmd
